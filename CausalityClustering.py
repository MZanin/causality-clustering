#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Companion software for:
 
M. Zanin
Simplifying functional network interpretation through causality clustering
"""


from statsmodels.tsa.tsatools import lagmat2ds
from statsmodels.regression.linear_model import OLS
from statsmodels.tools.tools import add_constant

import numpy as np

import warnings






def Get2NodesMotif( TS, maxlag, alpha = 0.01, isolFilter = False, preAssignment = [] ):
    
    numNodes = np.size( TS, 0 )
    tsLen = np.size( TS, 1 )
    
    numClusters = 2
    
    Motif = np.array( [ [ 0, 1 ], [ 0, 0 ] ] )
    
    bestAssign = []
    bestJ = 1.0
    
    pvFilter = CalculateIsolatedNodes( alpha, numNodes, TS, maxlag, isolFilter )        
    cAssignment = np.zeros( ( numNodes ) )
    
    allAssignments = []
    
    while True:
        
        validCombination = True
                        
        fAssignment = np.copy( cAssignment )
        fAssignment[ pvFilter ] = -1
        
        if np.size( preAssignment ) > 0:
            for k in range( np.size( preAssignment ) ):
                fAssignment[ k ] = preAssignment[ k ]
        
        for k in range( numClusters ):
            if np.sum( fAssignment == k ) == 0:
                validCombination = False
        
        
        if validCombination and isolFilter and len( allAssignments ) > 2:
            if np.sum( np.all( fAssignment == allAssignments, axis = 1 ) ) > 0:
                validCombination = False
                        
        
        if validCombination:
        
            subTS = np.zeros( ( numClusters, tsLen ) )
            for k in range( numClusters ):
                subTS[ k, : ] = np.sum( TS[ fAssignment == k, : ], axis = 0 )
            for k in range( numClusters ):
                subTS[ k, : ] /= np.max( np.abs( subTS[ k, : ] ) )
                
            if np.sum( np.isnan( subTS ) ) > 0:
                validCombination = False
                
                
                
        if validCombination:
            
            if isolFilter:
                allAssignments.append( fAssignment )
            
            
            AM = CreateAMFromTimeSeries( subTS, maxlag )
                    
            if np.sum( np.isnan( AM ) ) == 0:
                
                newJ = CalculateJ( Motif, AM )
                            
                if newJ < bestJ:
                    bestJ = newJ
                    bestAssign = np.copy( fAssignment )
                        
                    
                            
        completed, cAssignment = GetNextAssignment( cAssignment, \
                                 numNodes, numClusters, preAssignment )
        if completed:
            break
        

    
    return ( np.array( bestAssign ), bestJ )










def Get3NodesMotif( TS, maxlag, alpha = 0.01, isolFilter = True, preAssignment = [], \
                      Motif = np.array( [ [ 0, 1, 0 ], [ 0, 0, 1 ], [ 0, 0, 0 ] ] ) ):
    
    numNodes = np.size( TS, 0 )
    tsLen = np.size( TS, 1 )
    
    numClusters = 3
       
    bestAssign = []
    bestJ = 1.0
    
    pvFilter = CalculateIsolatedNodes( alpha, numNodes, TS, maxlag, isolFilter )        
    cAssignment = np.zeros( ( numNodes ) )
    
    allAssignments = []

    while True:
        
        validCombination = True
                        
        fAssignment = np.copy( cAssignment )
        fAssignment[ pvFilter ] = -1

        if np.size( preAssignment ) > 0:
            for k in range( np.size( preAssignment ) ):
                fAssignment[ k ] = preAssignment[ k ]
        
        for k in range( numClusters ):
            if np.sum( fAssignment == k ) == 0:
                validCombination = False
        
        
        if validCombination and isolFilter and len( allAssignments ) > 2:
            if np.sum( np.all( fAssignment == allAssignments, axis = 1 ) ) > 0:
                validCombination = False

        
        if validCombination:
        
            subTS = np.zeros( ( numClusters, tsLen ) )
            for k in range( numClusters ):
                subTS[ k, : ] = np.sum( TS[ fAssignment == k, : ], axis = 0 )
            for k in range( numClusters ):
                subTS[ k, : ] /= np.max( np.abs( subTS[ k, : ] ) )
                
            if np.sum( np.isnan( subTS ) ) > 0:
                validCombination = False
                
                
                
        if validCombination:
            
            if isolFilter:
                allAssignments.append( fAssignment )

            AM = CreateAMFromTimeSeries( subTS, maxlag )
                    
            if np.sum( np.isnan( AM ) ) == 0:
                
                newJ = CalculateJ( Motif, AM )

                if newJ < bestJ:
                    bestJ = newJ
                    bestAssign = np.copy( fAssignment )
                        
                    
                            
        completed, cAssignment = GetNextAssignment( cAssignment, \
                                 numNodes, numClusters, preAssignment )
        if completed:
            break
        

    
    return ( np.array( bestAssign ), bestJ )








def Get2NodesMotifs_JForOptimization( x, TS ):
    
    fAssignment = np.array( x )
    fAssignment[ fAssignment >= 0 ] = 1
    fAssignment[ fAssignment < 0 ] = 0
    fAssignment = np.array( fAssignment, dtype = int )
    
    tsLen = np.size( TS, 1 )
    
    maxlag = 5   
    numClusters = 2    
    Motif = np.array( [ [ 0, 1 ], [ 0, 0 ] ] )
    
                
    subTS = np.zeros( ( numClusters, tsLen ) )
    for k in range( numClusters ):
        subTS[ k, : ] = np.sum( TS[ fAssignment == k, : ], axis = 0 )
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore")
        for k in range( numClusters ):
            subTS[ k, : ] /= np.max( np.abs( subTS[ k, : ] ) )
        
    if np.sum( np.isnan( subTS ) ) > 0:
        return 1.0
                
                        
    AM = CreateAMFromTimeSeries( subTS, maxlag )
    if np.sum( np.isnan( AM ) ) > 0:
        return 1.0
            
    newJ = CalculateJ( Motif, AM )
                                                
    return newJ







def CalculateJ( Motif, AM ):
    
    numClusters = Motif.shape[0]
    J = 1.0
    for n1 in range( numClusters ):
        for n2 in range( numClusters ):
            if n1 == n2:
                continue
            
            if Motif[n1, n2] == 0:
                J *= ( 1.0 - AM[ n1, n2 ] )
            else:
                J *= AM[ n1, n2 ]
    
    return J




def CreateAMFromTimeSeries( subTS, maxlag ):
    
    numClusters = subTS.shape[0]
    AM = np.zeros( ( numClusters, numClusters ) )
    for n1 in range( numClusters ):
        for n2 in range( numClusters ):
            
            if n1 == n2:
                AM[ n1, n2 ] = 1.0
                continue
            
            with warnings.catch_warnings():
                warnings.filterwarnings("ignore")
                AM[ n1, n2 ] = GT_MultiLag( subTS[ n1, : ], subTS[ n2, : ], maxlag )[0]
    
    return AM



def GetNextAssignment( cAssignment, numNodes, numClusters, preAssignment ):
    
    completed = False
    cAssignment[ -1 ] += 1
    for k in range( numNodes - 1, -1, -1 ):
        if cAssignment[ k ] >= numClusters:
            if k == 0:
                completed = True
                break
            if np.size( preAssignment ) > 0 and k <= np.size( preAssignment ):
                completed = True
                break
            cAssignment[ k - 1 ] += 1
            cAssignment[ k ] = 0
    return completed, cAssignment





def CalculateIsolatedNodes( alpha, numNodes, TS, maxlag, isolFilter ):
    
    pvFilter = np.zeros( (numNodes), dtype = bool )
    if not isolFilter:
        return pvFilter

    effAlpha = 1.0 - np.power( ( 1.0 - alpha ), 1.0 / ( numNodes * ( numNodes - 1 ) ) )    

    pvMatrix = np.ones( (numNodes, numNodes) )
    for n1 in range( numNodes ):
        for n2 in range( numNodes ):
            pvMatrix[n1, n2] = GT_MultiLag( TS[ n1, : ], TS[ n2, : ], maxlag )[0]
            
    if isolFilter:
        
        for n1 in range( numNodes ):
            if np.sum( pvMatrix[n1, :] < alpha ) + np.sum( pvMatrix[:, n1] < effAlpha ) == 0:
                pvFilter[n1] = True

    return pvFilter




def GT_MultiLag( X, Y, maxlag ):
    
    fullTS = np.array( [Y, X] ).T

    allPValues = np.zeros( (maxlag, 1) )    
    
    topLag = maxlag
    for maxlag in range(1, topLag+1):    
    
        dta = lagmat2ds(fullTS, maxlag, trim='both', dropex=1)
        dtajoint = add_constant(dta[:,1:], prepend=False)
    
        res2djoint = OLS(dta[:,0], dtajoint).fit()
        
        rconstr = np.column_stack((np.zeros((maxlag-1,maxlag-1)), np.eye(maxlag-1, maxlag-1),\
                                       np.zeros((maxlag-1, 1))))
        rconstr = np.column_stack((np.zeros((maxlag,maxlag)), np.eye(maxlag, maxlag),\
                                       np.zeros((maxlag, 1))))
        ftres = res2djoint.f_test(rconstr)
        allPValues[maxlag-1] = np.squeeze(ftres.pvalue)[()]
    
    return np.min( allPValues ), allPValues
