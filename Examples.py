#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Companion software for:
 
M. Zanin
Simplifying functional network interpretation through causality clustering
"""

import numpy as np
import CausalityClustering as gc



## ------------------------------------------------
## Simple clustering, with four nodes in two clusters
## ------------------------------------------------

numNodes = 4 # Number of nodes (or time series)
gamma = 1.0 # Coupling between the time series

# Here we create the time series, as random sequences; we then couple
# the last two to the first two, through gamma
TS = np.random.normal( 0.0, 1.0, ( numNodes, 500 ) )        
TS[ int(numNodes / 2):, 2:] += gamma * TS[:int(numNodes / 2), :-2]

( clusters, bestJ ) = gc.Get2NodesMotif( TS, maxlag = 5 )

# Two variables are returned:
# 1. clusters, which define the assignation of nodes to clusters. The result should
# be something like [0, 0, 1, 1]; yet, as the series are random, at times this
# may fail...
# 2. The J for the best solution found.





## ------------------------------------------------
## Same clustering, with weaker coupling
## ------------------------------------------------

numNodes = 4
gamma = 0.2 # Notice how now the coupling is much weaker...

TS = np.random.normal( 0.0, 1.0, ( numNodes, 500 ) )        
TS[ int(numNodes / 2):, 2:] += gamma * TS[:int(numNodes / 2), :-2]

( clusters2, bestJ2 ) = gc.Get2NodesMotif( TS, maxlag = 5 )

# In this case, clusters2 may return different clusters, different from the
# one we might expect. As the coupling is weaker, results are less clear.
# Also, here J should be higher than what previously obtained.






## ------------------------------------------------
## Six nodes and three clusters
## ------------------------------------------------

gamma = 1.0 # Coupling between the time series

# Here we say that nodes 4 and 5 are driven by nodes 2 and 3; and
# nodes 2 and 3 by nodes 0 and 1
TS = np.random.normal( 0.0, 1.0, ( 6, 500 ) )        
TS[ 4:6, 2:] += gamma * TS[2:4, :-2]
TS[ 2:4, 2:] += gamma * TS[:2, :-2]

( clusters3, bestJ3 ) = gc.Get3NodesMotif( TS, maxlag = 5 )
# The resulting clusters should be something like [0, 0, 1, 1, 2, 2]






## ------------------------------------------------
## Clustering using an annealing optimization, i.e. when brute force is too much!
## ------------------------------------------------

from scipy.optimize import dual_annealing
# We will need this function to perform the actual optimisation

numNodes = 4 
gamma = 1.0 
TS = np.random.normal( 0.0, 1.0, ( numNodes, 500 ) )        
TS[ int(numNodes / 2):, 2:] += gamma * TS[:int(numNodes / 2), :-2]
# Time series created as usual

bounds = np.zeros( (numNodes, 2) )
bounds[ :, 0 ] = -1.5
bounds[ :, 1 ] = 1.5
# Here we defined the bounds, i.e. the interval in which the solution has to be found.
# Note that we use the convention <0 is the first cluster, and >0 the second one.
# So, we set that the solutions must be bound between -1.5 and 1.5

x0 = np.random.binomial( 1, 0.5, (numNodes) ) - 0.5
# Initial random conditions, in this case random values -0.5 or 0.5
            
ret = dual_annealing( gc.Get2NodesMotifs_JForOptimization, bounds = bounds, \
                      args = [TS], maxiter = 10, x0 = x0 )
# Here we execute the optimization

bestJ = ret['fun']
bestRet = np.copy( ret['x'] )
# We extract the best J and the best clustering.
# Note that you may need the J to perform this optimization several times,
# and retrieve the best execution.
        
clusters4 = bestRet
clusters4[ clusters4 >= 0 ] = 1
clusters4[ clusters4 < 0 ] = 0
clusters4 = np.array( clusters4, dtype = int )
# Finally, we transform back the solution as values 0 and 1.

