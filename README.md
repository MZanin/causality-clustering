Welcome to the library associated to the paper:

Zanin, M. (2021).
Simplifying functional network representation and interpretation through causality clustering.
Scientific Reports, 11(1), 1-12.
https://www.nature.com/articles/s41598-021-94797-y



You will find here three files:
1. CausalityClustering.py, the main library.
2. Examples.py, a set of simple examples that will let you recover the results presented in the paper. Start here if you are not sure how to use this library!
3. test_CausalityClustering.py, a file for unit testing the library.

Additions, bug reports and ideas are welcome!
