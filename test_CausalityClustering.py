#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Companion software for:
 
M. Zanin
Simplifying functional network interpretation through causality clustering
"""

import numpy as np
import unittest

import CausalityClustering as gc



class TestStringMethods(unittest.TestCase):



    def test_J_2_Clusters(self):
        
        motif = np.array( [ [0, 1], [0, 0] ] )
        AM = np.array( [ [0.5, 0.5], [0.5, 0.5] ] )
        J = gc.CalculateJ( motif, AM )
        self.assertEqual( J, 0.25 )
        
        motif = np.array( [ [0, 1], [0, 0] ] )
        AM = np.array( [ [0.25, 0.75], [0.25, 0.25] ] )
        J = gc.CalculateJ( motif, AM )
        self.assertEqual( J, 0.5625 )
    
    
    
    def test_GetNextAssignment(self):
        
        cAssignment = np.array( [1, 0, 1, 1] )
        numNodes = 4
        numClusters = 2
        preAssignment = []
        completed, Result = gc.GetNextAssignment( cAssignment, numNodes, numClusters, preAssignment )
        expResult = np.array( [1, 1, 0, 0] )
        self.assertEqual( np.sum( Result == expResult ), 4 )
        self.assertFalse( completed )
        
        cAssignment = np.array( [1, 1, 1, 1] )
        numNodes = 4
        numClusters = 2
        preAssignment = []
        completed, Result = gc.GetNextAssignment( cAssignment, numNodes, numClusters, preAssignment )
        self.assertTrue( completed )
        
        
        
    def test_CheckIsolatedNodes(self):
        
        isolFilter = False
        alpha = 0.0001
        numNodes = 1
        TS = np.random.uniform( 0.0, 1.0, (numNodes, 500) )
        maxlag = 5
        pvFilter = gc.CalculateIsolatedNodes( alpha, numNodes, TS, maxlag, isolFilter )
        expResult = np.zeros( (numNodes) )
        self.assertEqual( np.sum( pvFilter == expResult ), numNodes )
        
        isolFilter = True
        alpha = 0.00001
        numNodes = 4
        TS = np.random.uniform( 0.0, 1.0, (numNodes, 500) )
        maxlag = 5
        pvFilter = gc.CalculateIsolatedNodes( alpha, numNodes, TS, maxlag, isolFilter )
        expResult = np.ones( (numNodes) )
        self.assertEqual( np.sum( pvFilter == expResult ), numNodes )



    def test_Clustering_4_Nodes_Into_2_Clusters(self):
        
        numNodes = 4
        gamma = 1.0
        TS = np.random.normal( 0.0, 1.0, ( numNodes, 500 ) )        
        TS[ int(numNodes / 2):, 2:] += gamma * TS[:int(numNodes / 2), :-2]
    
        expResult = np.array( [0, 0, 1, 1] )
        
        ( clusters, bestJ ) = \
            gc.Get2NodesMotif( TS, maxlag = 5, alpha = 0.01, isolFilter = False )

        self.assertEqual( np.sum( clusters == expResult ), 4 )
    


    def test_Clustering_6_Nodes_Into_3_Clusters_A(self):
        
        gamma = 1.0
        TS = np.random.normal( 0.0, 1.0, ( 6, 500 ) )        
        TS[ 4:6, 2:] += gamma * TS[2:4, :-2]
        TS[ 2:4, 2:] += gamma * TS[:2, :-2]
        Motif = np.array( [ [ 0, 1, 0 ], [ 0, 0, 1 ], [ 0, 0, 0 ] ] )
        
        ( clusters, bestJ ) = \
            gc.Get3NodesMotif( TS, maxlag = 5, alpha = 0.01, isolFilter = False, \
                               Motif = Motif )
        
        expResult = np.zeros( (6) )
        expResult[ 2:4 ] = 1
        expResult[ 4:6 ] = 2
            
        self.assertEqual( np.sum( clusters == expResult ), 6 )
    
    

    def test_Clustering_6_Nodes_Into_3_Clusters_B(self):
        
        gamma = 1.0
        TS = np.random.normal( 0.0, 1.0, ( 6, 500 ) )        
        TS[ 4:6, 2:] += gamma * TS[2:4, :-2]
        TS[ 2:4, 2:] += gamma * TS[:2, :-2]
        
        ( clusters, bestJ ) = \
            gc.Get3NodesMotif( TS, maxlag = 5, alpha = 0.01, isolFilter = False, \
                               Motif = np.array( [ [ 0, 1, 1 ], [ 0, 0, 1 ], [ 0, 0, 0 ] ] ) )
        
        expResult = np.zeros( (6) )
        expResult[ 2:4 ] = 1
        expResult[ 4:6 ] = 2
            
        self.assertEqual( np.sum( clusters == expResult ), 6 )





if __name__ == '__main__':
    
    unittest.main()

